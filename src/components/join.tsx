import { useContext, useEffect } from "react";
import { RoomContext, useRoomstate } from "../context/roomcontext"

export const Join : React.FC = ()=>{
    const {ws} = useRoomstate();

     const createRoom = ()=>{
        ws.emit("create-room")
     }

    return   <button onClick={createRoom} className='bg-rose-400 py-2 px-8 rounded-lg text-lg hover:bg-rose-600 text-white'>Start new Button</button>
}