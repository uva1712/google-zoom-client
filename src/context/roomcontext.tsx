import React, { createContext, useContext, useEffect, useState, useReducer } from "react";
import socketio from "socket.io-client";
import {useNavigate} from "react-router-dom";
import Peer from "peerjs";
import { v4 as uuid4 } from "uuid";
import { peerReducer } from "./peerReducer";
import { addPeerAction, removePeerAction } from "./peerActions";

const WS = "http://192.168.79.3:8080";

export const RoomContext = createContext<null | any>(null);

const ws = socketio(WS);

interface RoomProviderProps {
    children: React.ReactNode;
  }

export const RoomProvider: React.FC<RoomProviderProps> = ({children})=>{
const [me,setMe] = useState<Peer>();
const [stream,setStream] = useState<MediaStream>();
const [peers, dispatch] = useReducer(peerReducer,{})
const navigate = useNavigate();

   const enterRoom = ({roomId} : {roomId : string})=>{
         console.log("Room Id is ",roomId);
         navigate(`/room/${roomId}`)
   }

   const getUsers = ({participants}:{participants:string[]})=>{
     console.log("The participants ",participants)
   }

   useEffect(()=>{
    const meId = uuid4();
    let peer = new Peer(meId);
    setMe(peer);
    try {
      navigator.mediaDevices.getUserMedia({video:true,audio:true}).then((stream)=>{
        console.log('Media devices access granted.');
        setStream(stream);
      })
    } catch (error) {
      console.log(error)
    }
    ws.on("room-created",enterRoom);
    ws.on("get-users",getUsers)
   },[ws])


     useEffect(()=>{
      if(!me) return;
      if(!stream) return;
      ws.on("users-joined",({peerId})=>{
           const call = me.call(peerId,stream);
           call.on("stream",(peerStream)=>{
            console.log("The user joined ",peerStream)
            dispatch(addPeerAction(peerId,peerStream))
           })
      })

      me.on('call',(call)=>{
        call.answer(stream);
        call.on("stream",(peerStream)=>{
          console.log("Answering the call ",peerStream)
          dispatch(addPeerAction(call.peer,peerStream))
         })
      })
     },[me,stream])

     ws.on("user-disconnected",(peerId : string)=>{
       console.log("The disconnected user is ",peerId)
       dispatch(removePeerAction(peerId))
     })

   console.log("The peers are ",peers)

   return <RoomContext.Provider value={{ws,me, stream, peers}}>
        {children}
    </RoomContext.Provider>
}


export const useRoomstate = () => useContext(RoomContext); 