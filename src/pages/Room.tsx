import { useEffect, useReducer } from "react";
import {useParams} from "react-router-dom"
import { useRoomstate } from "../context/roomcontext";
import { VideoPlayer } from "../components/videoplayer";
import { PeerState } from "../context/peerReducer";

export const Room = ()=>{
    const {id} = useParams();
    const {ws,me,stream,peers} =  useRoomstate();
   

    useEffect(()=>{
      if(me){
        ws.emit("join-room",{roomId : id,peerId : me._id})
      }
      
    },[id,ws,me])
    return <>
      Room Id {id}
      <VideoPlayer stream={stream} />
      {Object.values(peers as PeerState).map((peer)=>{
        return <VideoPlayer stream={peer.stream} />
      })}
    </>
}